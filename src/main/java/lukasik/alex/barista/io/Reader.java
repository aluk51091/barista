/**
 * 
 */
package lukasik.alex.barista.io;

/**
 * @author Alex
 *
 */
public interface Reader {
	public String read();
}
