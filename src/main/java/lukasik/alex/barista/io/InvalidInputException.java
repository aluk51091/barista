/**
 * 
 */
package lukasik.alex.barista.io;

/**
 * @author Alex
 *
 */
public class InvalidInputException extends RuntimeException {

	private static final long serialVersionUID = -7807653333675373929L;
	
	public InvalidInputException(String message){
		super(message);
	}
	
	public InvalidInputException(){
		super();
	}

}
