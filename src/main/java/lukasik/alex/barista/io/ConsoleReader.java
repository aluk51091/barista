/**
 * 
 */
package lukasik.alex.barista.io;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * @author Alex
 *
 */
@Service
public class ConsoleReader implements Reader {
	private static final Logger logger = LogManager.getLogger(ConsoleReader.class);
	
	protected Scanner sc = new Scanner(System.in);
	/* (non-Javadoc)
	 * @see lukasik.alex.barista.io.Reader#readLine()
	 */
	@Override
	public String read() {
        String input = sc.nextLine();
        logger.debug(input);
        return input;
	}

}
