/**
 * 
 */
package lukasik.alex.barista.processor;

import lukasik.alex.barista.inventory.Drink;
import lukasik.alex.barista.inventory.Menu;
import lukasik.alex.barista.inventory.dao.api.InventoryDao;
import lukasik.alex.barista.io.InvalidInputException;
import lukasik.alex.barista.menu.MenuPrinter;
import lukasik.alex.barista.processor.api.Processor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Alex
 *
 */
@Service("integerProcessor")
public class IntegerProcessor implements Processor<Integer> {
	private static final Logger logger = LogManager.getLogger(IntegerProcessor.class);
	
	@Autowired
	private InventoryDao inventoryDao;
	
	@Autowired
	private MenuPrinter menuPrinter;
	
	/* (non-Javadoc)
	 * @see lukasik.alex.barista.services.Processor#process(java.lang.Object)
	 */
	@Override
	public void process(Integer index) {
		logger.debug("Processing with: {}",index);
		if(index <= Menu.getDrinkMenu().size()){
			//valid size
			Drink drink = Menu.getDrinkAt(index);
			if(inventoryDao.isDrinkInStock(drink)){
				logger.debug("removing: {}", drink);
				inventoryDao.removeInventory(drink);
				menuPrinter.printDispensing(drink);
			}else{
				throw new InvalidInputException(drink.toString() + " is out of stock!");
			}
		}else{
			throw new InvalidInputException(index + ": is not an option");
		}

	}

	public InventoryDao getInventoryDao() {
		return inventoryDao;
	}

	public void setInventoryDao(InventoryDao inventoryDao) {
		this.inventoryDao = inventoryDao;
	}

	public void setMenuPrinter(MenuPrinter menuPrinter) {
		this.menuPrinter = menuPrinter;
	}
	
	

}
