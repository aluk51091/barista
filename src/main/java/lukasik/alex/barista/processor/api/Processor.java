/**
 * 
 */
package lukasik.alex.barista.processor.api;

/**
 * @author Alex
 *
 */
public interface Processor<T> {
	public void process(T obj);
}
