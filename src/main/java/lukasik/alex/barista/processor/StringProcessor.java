/**
 * 
 */
package lukasik.alex.barista.processor;

import lukasik.alex.barista.inventory.dao.api.InventoryDao;
import lukasik.alex.barista.io.InvalidInputException;
import lukasik.alex.barista.processor.api.Processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Alex
 *
 */
@Service("stringProcessor")
public class StringProcessor implements Processor<String> {
	@Autowired
	private InventoryDao inventoryDao;
	
	/* (non-Javadoc)
	 * @see lukasik.alex.barista.services.Processor#process(java.lang.Object)
	 */
	@Override
	public void process(String input) {
		String in = input.toLowerCase();
		if(in.equals("r")){
			inventoryDao.restock();
		}else if(in.equals("q")){
			System.exit(0);
		}else{
			//throws invalid input exception
			throw new InvalidInputException();
		}

	}

	public void setInventoryDao(InventoryDao inventoryDao) {
		this.inventoryDao = inventoryDao;
	}

}
