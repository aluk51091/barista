/**
 * 
 */
package lukasik.alex.barista.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Alex
 *
 */
public class IntegerUtilities {
	
	private static Logger logger = LogManager.getLogger(IntegerUtilities.class);
	
	public static boolean isInteger(String val) {
		logger.debug("isInteger(String val) is called with: {}",val);
		try {
			Integer.parseInt(val);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	public static Integer parseInteger(String input) {
		logger.debug("parseInteger(String input) is called with: {}",input);
		Integer val = null;
		try {
			val = Integer.parseInt(input);
		} catch (Exception e) {
		}
		return val;
	}
}
