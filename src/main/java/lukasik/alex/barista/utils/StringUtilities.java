/**
 * 
 */
package lukasik.alex.barista.utils;

import org.apache.commons.lang3.text.WordUtils;

/**
 * @author Alex
 *
 */
public class StringUtilities {
	/**
	 * converts _ to spaces and title cases the input
	 * @param input
	 * @return
	 */
	public static String upperUnderscoreToCamelSpaced(String input){
		if(input == null){
			return null;
		}
		return WordUtils.capitalize(input.toLowerCase().replaceAll("_", " "));
	}
}
