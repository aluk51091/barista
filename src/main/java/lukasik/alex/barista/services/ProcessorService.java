/**
 * 
 */
package lukasik.alex.barista.services;

import lukasik.alex.barista.io.InvalidInputException;
import lukasik.alex.barista.processor.api.Processor;
import lukasik.alex.barista.utils.IntegerUtilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author Alex
 *
 */
@Service
public class ProcessorService {
	private static final Logger logger = LogManager.getLogger(ProcessorService.class);
	
	@Autowired
	@Qualifier("stringProcessor")
	protected Processor<String> stringProcessor;
	
	@Autowired
	@Qualifier("integerProcessor")
	protected Processor<Integer> integerProcessor;
	
	public void process(String input){
		logger.debug("entered process(String input) with value: {}",input);
		if(IntegerUtilities.isInteger(input)){
			logger.debug("input is integer");
			integerProcessor.process(IntegerUtilities.parseInteger(input));
		}else if(input.length() == 1){
			logger.debug("input is 1 character string");
			stringProcessor.process(input);
		}else{
			logger.debug("exception is thrown");
			throw new InvalidInputException();
		}
	}

	
	// ---- ACCESSORS and MUTATORS (should be replaced with lombok ---- //

	public void setStringProcessor(Processor<String> stringProcessor) {
		this.stringProcessor = stringProcessor;
	}

	public void setIntegerProcessor(Processor<Integer> integerProcessor) {
		this.integerProcessor = integerProcessor;
	}
}
