/**
 * 
 */
package lukasik.alex.barista;

import lukasik.alex.barista.io.InvalidInputException;
import lukasik.alex.barista.io.Reader;
import lukasik.alex.barista.menu.MenuPrinter;
import lukasik.alex.barista.services.ProcessorService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Alex
 *
 */
@Service
public class AutomatedBarista {
	private static final Logger logger = LogManager.getLogger(AutomatedBarista.class);
	
	@Autowired
	private MenuPrinter menuPrinter;
	
	@Autowired
	private Reader reader;

	@Autowired
	private ProcessorService processorService;
	
	public void initialize(){
		String input;
		do{
			try{
				menuPrinter.printIngredientMenu();
				menuPrinter.printDrinkMenu();
				input = reader.read();
				processorService.process(input);
			}catch(InvalidInputException e){
				logger.error("Invalid Input: {}",e);
				System.out.println("Invalid Input, Try Again");
			}			
		}while(true);
	}

	public MenuPrinter getMenuPrinter() {
		return menuPrinter;
	}

	public void setMenuPrinter(MenuPrinter menuPrinter) {
		this.menuPrinter = menuPrinter;
	}

	public Reader getReader() {
		return reader;
	}

	public void setReader(Reader reader) {
		this.reader = reader;
	}

	public ProcessorService getProcessorService() {
		return processorService;
	}

	public void setProcessorService(ProcessorService processorService) {
		this.processorService = processorService;
	}
	
}
