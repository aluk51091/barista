/**
 * 
 */
package lukasik.alex.barista.menu;

import lukasik.alex.barista.inventory.Drink;

/**
 * @author Alex
 *
 */
public interface MenuPrinter {
	public void printDrinkMenu();
	public void printIngredientMenu();
	public void printDispensing(Drink drink);
	
}
