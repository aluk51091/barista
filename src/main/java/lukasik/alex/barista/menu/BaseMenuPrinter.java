/**
 * 
 */
package lukasik.alex.barista.menu;

import lukasik.alex.barista.inventory.Drink;
import lukasik.alex.barista.inventory.Ingredient;
import lukasik.alex.barista.inventory.Menu;
import lukasik.alex.barista.inventory.dao.api.InventoryDao;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Alex
 *
 */
public abstract class BaseMenuPrinter implements MenuPrinter {

	@Autowired
	private InventoryDao inventoryDao;

	protected String generateIngredientMenu() {
		StringBuilder sb = new StringBuilder();
		sb.append("Inventory:\n\n");
		for (Ingredient ingredient : Menu.getIngredientMenu()) {
			sb.append(ingredient.getName()).append(",")
					.append(inventoryDao.getIngredientQuantity(ingredient))
					.append("\n\n");
		}
		return sb.toString();
	}

	protected String generateDrinkMenu() {
		StringBuilder sb = new StringBuilder();
		int i = 1;
		sb.append("Menu:\n\n");
		for (Drink drink : Menu.getDrinkMenu()) {
			sb.append(i++).append(",").append(drink.getName()).append(",")
					.append(drink.getDrinkPrice()).append(",")
					.append(inventoryDao.isDrinkInStock(drink)).append("\n\n");
		}
		return sb.toString();
	}

	public void setInventoryDao(InventoryDao inventoryDao) {
		this.inventoryDao = inventoryDao;
	}
	
	public String generateDispense(Drink drink){
		StringBuilder sb = new StringBuilder();
		sb.append("Dispensing: ").append(drink.toString()).append("\n\n");
		return sb.toString();
	}

}
