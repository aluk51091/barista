/**
 * 
 */
package lukasik.alex.barista.menu.impl;

import lukasik.alex.barista.inventory.Drink;
import lukasik.alex.barista.menu.BaseMenuPrinter;

import org.springframework.stereotype.Service;

/**
 * @author Alex
 *
 */
@Service
public class ConsoleMenuPrinter extends BaseMenuPrinter {

	/* (non-Javadoc)
	 * @see lukasik.alex.barista.menu.MenuPrinter#printIngredientMenu()
	 */
	@Override
	public void printIngredientMenu() {
		System.out.print(this.generateIngredientMenu());
	}

	/* (non-Javadoc)
	 * @see lukasik.alex.barista.menu.BaseMenuPrinter#printDrinkMenu()
	 */
	@Override
	public void printDrinkMenu() {
		System.out.print(this.generateDrinkMenu());
	}

	@Override
	public void printDispensing(Drink drink) {
		System.out.print(this.generateDispense(drink));
	}

}
