/**
 * 
 */
package lukasik.alex.barista.inventory;

import java.math.BigDecimal;
import java.util.List;

import lukasik.alex.barista.utils.StringUtilities;

import com.google.common.collect.ImmutableList;

/**
 * @author Alex
 *
 */
public enum Drink {
	/* @formatter:off */
	COFFEE(Ingredient.COFFEE, Ingredient.COFFEE, Ingredient.COFFEE, Ingredient.SUGAR, Ingredient.CREAM),
	DECAF_COFFEE(Ingredient.DECAF_COFFEE, Ingredient.DECAF_COFFEE, Ingredient.DECAF_COFFEE, Ingredient.SUGAR, Ingredient.CREAM),
	CAFFE_LATTEE(Ingredient.ESPRESSO, Ingredient.ESPRESSO, Ingredient.STEAMED_MILK),
	CAFFE_AMERICANO(Ingredient.ESPRESSO, Ingredient.ESPRESSO, Ingredient.ESPRESSO),
	CAFFE_MOCHA(Ingredient.ESPRESSO, Ingredient.COCOA, Ingredient.STEAMED_MILK, Ingredient.WHIPPED_CREAM),
	CAPPUCCINO(Ingredient.ESPRESSO,Ingredient.ESPRESSO, Ingredient.COCOA, Ingredient.STEAMED_MILK, Ingredient.FOAMED_MILK);
	/* @formatter:on */

	private final List<Ingredient> ingredients;
	private final String name;
	private Drink(Ingredient... ingredients) {
		this.ingredients = ImmutableList.copyOf(ingredients).asList();
		this.name = StringUtilities.upperUnderscoreToCamelSpaced(this.name());
	}
	
	//---- ACCESSORS ----//
	public String getName(){
		return this.name;
	}
	
	public List<Ingredient> getIngredients(){
		return this.ingredients;
	}
	//---- ACCESSORS ----//
	
	public BigDecimal getDrinkPrice(){
		return this.getIngredients().stream().map(Ingredient::getCost).reduce(BigDecimal.ZERO, BigDecimal::add);
	}
	
	@Override
	public String toString(){
		return this.getName();
	}
}
