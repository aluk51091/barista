/**
 * 
 */
package lukasik.alex.barista.inventory;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import lukasik.alex.barista.io.InvalidInputException;

import com.google.common.collect.ImmutableList;

/**
 * @author Alex
 *
 */
public class Menu {
	private static final List<Drink> drinkMenu;
	private static final List<Ingredient> ingredientMenu;

	static {
		drinkMenu = ImmutableList.copyOf(Arrays.stream(Drink.values())
				.sorted(Comparator.comparing(Drink::getName))
				.collect(Collectors.toList()));
		ingredientMenu = ImmutableList.copyOf(Arrays
				.stream(Ingredient.values())
				.sorted(Comparator.comparing(Ingredient::getName))
				.collect(Collectors.toList()));
	}

	public static List<Drink> getDrinkMenu() {
		return drinkMenu;
	}

	public static List<Ingredient> getIngredientMenu() {
		return ingredientMenu;
	}

	/**
	 * Gets the drink that is located at the index this method offsets the
	 * number by 1
	 * 
	 * @param i
	 *            - value that should be 1 - 6
	 * @return The Drink located at that index
	 */
	public static Drink getDrinkAt(int i) {
		if (i < 1 || drinkMenu.size() < i) {
			throw new InvalidInputException("Drink Option not in Range");
		}
		return drinkMenu.get(i - 1);
	}

	public static Ingredient getIngredientAt(int i) {
		if (i < 1 || ingredientMenu.size() < i) {
			throw new InvalidInputException("Ingredient Option not in Range");
		}
		return ingredientMenu.get(i - 1);
	}
}
