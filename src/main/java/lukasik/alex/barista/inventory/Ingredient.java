/**
 * 
 */
package lukasik.alex.barista.inventory;

import java.math.BigDecimal;
import java.math.MathContext;

import lukasik.alex.barista.utils.StringUtilities;

/**
 * @author Alex - Ingredients are enums. This makes it easier to work with in
 *         code, especially if there is no database backend. If you want to add
 *         an ingredient or alter the price you only need to alter it in here.
 */
public enum Ingredient {
	/* @formatter:off */
	COFFEE(".75"),
	DECAF_COFFEE(".75"),
	SUGAR(".25"),
	CREAM(".25"),
	STEAMED_MILK(".35"),
	FOAMED_MILK(".35"),
	ESPRESSO("1.10"),
	COCOA(".90"),
	WHIPPED_CREAM("1.00");
	/* @formatter:on */

	private final String name;
	private final BigDecimal cost;

	private Ingredient(String drinkCost) {
		this.cost = new BigDecimal(drinkCost, MathContext.DECIMAL32);
		this.name = StringUtilities.upperUnderscoreToCamelSpaced(this.name());
	}

	public String getName() {
		return name;
	}

	public BigDecimal getCost() {
		return cost;
	}
	
	@Override
	public String toString(){
		return this.getName();
	}

}
