/**
 * 
 */
package lukasik.alex.barista.inventory.dao.api;

import lukasik.alex.barista.inventory.Drink;
import lukasik.alex.barista.inventory.Ingredient;

/**
 * @author Alex
 *
 */
public interface InventoryDao {
	public void restock();
	public Integer getIngredientQuantity(Ingredient ingredient);
	public Boolean isDrinkInStock(Drink drink);
	public void removeInventory(Drink drink);
}
