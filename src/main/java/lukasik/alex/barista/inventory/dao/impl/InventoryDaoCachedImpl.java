/**
 * 
 */
package lukasik.alex.barista.inventory.dao.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import lukasik.alex.barista.inventory.Drink;
import lukasik.alex.barista.inventory.Ingredient;
import lukasik.alex.barista.inventory.dao.api.InventoryDao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * @author Alex
 *
 */
@Service
public class InventoryDaoCachedImpl implements InventoryDao {
	/**
	 * static inventory size
	 */
	protected static final int maxInventory = 10;

	private final Logger logger = LogManager
			.getLogger(InventoryDaoCachedImpl.class);

	/**
	 * inventory is created on startup
	 */
	private Map<Ingredient, Integer> inventory = new HashMap<Ingredient, Integer>();

	/**
	 * Runs on post construct of object. This guarantees that every ingredient
	 * is in the map. For example strings can not be passed into the map
	 * ---public for testing purposes
	 */
	@PostConstruct
	protected void initializeInventory() {
		Arrays.stream(Ingredient.values()).forEach(t -> inventory.put(t, maxInventory));
	}

	@Override
	public void removeInventory(Drink drink) {
		this.removeInventory(drink.getIngredients());
	}

	@Override
	public Integer getIngredientQuantity(Ingredient ingredient) {
		return inventory.get(ingredient);
	}

	@Override
	public void restock() {
		this.initializeInventory();
	}

	@Override
	public Boolean isDrinkInStock(Drink drink) {
		return drink == null ? false : this.areIngredientsInStock(drink
				.getIngredients());
	}

	public void removeInventory(List<Ingredient> ingredients) {
		Map<Ingredient, Integer> reducedList = this.reduceList(ingredients);
		logger.debug("List was reduced to: {}", reducedList);

		this.subtractIngredient(reducedList);

		// I would return the new totals but doesn't state in
		// directions to do that so logging for development purposes

		logger.debug("Subtracting: {}", reducedList);
		logger.debug("new totals: {}", inventory);
	}

	protected Map<Ingredient, Integer> reduceList(List<Ingredient> ingredients) {
		logger.debug("Reducing ingredient list to a map: {}", ingredients);

		return ingredients.stream().collect(
				Collectors.toMap(i -> i, i -> 1, Integer::sum));
	}

	protected void subtractIngredient(Map<Ingredient, Integer> ingredientAmounts) {
		for (Map.Entry<Ingredient, Integer> ingredient : ingredientAmounts
				.entrySet()) {
			if (inventory.containsKey(ingredient.getKey())) {
				inventory.put(
						ingredient.getKey(),
						inventory.get(ingredient.getKey())
								- ingredient.getValue());
			}
		}
	}

	protected Boolean areIngredientsInStock(List<Ingredient> ingredients) {
		if (ingredients == null) {
			return false;
		}

		Map<Ingredient, Integer> reducedList = this.reduceList(ingredients);

		logger.debug("List was reduced to: {}", reducedList);

		for (Map.Entry<Ingredient, Integer> entry : reducedList.entrySet()) {
			if (this.inventory.containsKey(entry.getKey())) {
				Integer inventoryVal = this.inventory.get(entry.getKey());
				logger.debug(
						"Ingredient: {}, Inventory quantity: {}, Drink Required Quantity: {}",
						entry.getKey(), inventoryVal, entry.getValue());
				if (inventoryVal < entry.getValue()) {
					logger.debug("Not enough ingredients for this drink");
					return false;
				}
			}
		}
		return true;
	}

}
