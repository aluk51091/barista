/**
 * 
 */
package lukasik.alex.barista;

import lukasik.alex.barista.io.ConsoleReader;
import lukasik.alex.barista.io.InvalidInputException;
import lukasik.alex.barista.io.Reader;
import lukasik.alex.barista.menu.MenuPrinter;
import lukasik.alex.barista.menu.impl.ConsoleMenuPrinter;
import lukasik.alex.barista.services.ProcessorService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * @author Alex
 *
 */
public class AutomatedBaristaTest {
	private AutomatedBarista barista;
	@Before
	public void init(){
		barista = new AutomatedBarista();
		MenuPrinter printer = Mockito.spy(new ConsoleMenuPrinter());
		//gets rid of console logging
		Mockito.doNothing().when(printer).printDrinkMenu();
		Mockito.doNothing().when(printer).printIngredientMenu();
		barista.setMenuPrinter(printer);
	}
	
	@Test(expected=RuntimeException.class)
	public void multipleTests(){
		Reader r = Mockito.mock(ConsoleReader.class);
		Mockito.when(r.read()).thenReturn("-1","r","q");
		ProcessorService s = Mockito.mock(ProcessorService.class);
		barista.setReader(r);
		barista.setProcessorService(s);
		Mockito.doThrow(new InvalidInputException()).when(s).process("-1");
		//break out of infinite loop
		Mockito.doThrow(new RuntimeException()).when(s).process("q");
		barista.initialize();
	}
	
	@Test
	public void accessorTest(){
		Assert.assertNotNull(barista.getMenuPrinter());
		Assert.assertNull(barista.getProcessorService());
		Assert.assertNull(barista.getReader());
	}
}
