/**
 * 
 */
package lukasik.alex.barista.services;

import lukasik.alex.barista.io.InvalidInputException;
import lukasik.alex.barista.processor.IntegerProcessor;
import lukasik.alex.barista.processor.StringProcessor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;


/**
 * @author Alex
 *
 */
public class ProcessorServiceTest {
	ProcessorService service;
	
	@Before
	public void init(){
		service = new ProcessorService();
	}
	
	@Test
	public void processorIntegerTest(){
		IntegerProcessor spy = Mockito.spy(new IntegerProcessor());
		service.setIntegerProcessor(spy);
		Mockito.doNothing().when(spy).process((Integer)1);
		service.process("1");
		Mockito.verify(spy,Mockito.times(1)).process((Integer)1);
	}
	
	@Test
	public void processorStringTest(){
		StringProcessor spy = Mockito.spy(new StringProcessor());
		service.setStringProcessor(spy);
		Mockito.doNothing().when(spy).process("q");
		service.process("q");
		Mockito.verify(spy,Mockito.times(1)).process("q");
	}
	
	@Test(expected=InvalidInputException.class)
	public void processorStringTestInvalid(){
		service.process("asdf");
	}
	
	@Test
	public void processorInteger10Test(){
		IntegerProcessor spy = Mockito.spy(new IntegerProcessor());
		service.setIntegerProcessor(spy);
		Mockito.doNothing().when(spy).process((Integer)10);
		service.process("10");
		Mockito.verify(spy,Mockito.times(1)).process((Integer)10);
	}
}
