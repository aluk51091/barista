/**
 * 
 */
package lukasik.alex.barista.menu;

import lukasik.alex.barista.menu.impl.ConsoleMenuPrinter;

import org.junit.Test;
import org.mockito.Mockito;

/**
 * @author Alex
 *
 */
public class ConsoleMenuTest {

	@Test
	public void simpleCoverageTest(){
		ConsoleMenuPrinter printer = Mockito.spy(new ConsoleMenuPrinter());
		
		Mockito.doReturn("").when(printer).generateDrinkMenu();
		Mockito.doReturn("").when(printer).generateIngredientMenu();
		printer.printDrinkMenu();
		printer.printIngredientMenu();
	}
}
