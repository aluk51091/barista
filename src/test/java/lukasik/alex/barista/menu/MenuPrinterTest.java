/**
 * 
 */
package lukasik.alex.barista.menu;

import lukasik.alex.barista.inventory.dao.impl.BaseInventoryDaoCreation;
import lukasik.alex.barista.menu.impl.ConsoleMenuPrinter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Alex
 *
 */
public class MenuPrinterTest extends BaseInventoryDaoCreation {
	private BaseMenuPrinter printer;

	@Before
	public void init() {
		printer = new ConsoleMenuPrinter();
		printer.setInventoryDao(BaseInventoryDaoCreation.getDao());
	}

	@Test
	public void printIngredientTest() {
		Assert.assertEquals(printer.generateIngredientMenu(), "Inventory:\n\n"
				+ "Cocoa,10\n\n" + "Coffee,10\n\n" + "Cream,10\n\n"
				+ "Decaf Coffee,10\n\n" + "Espresso,10\n\n" + "Foamed Milk,10\n\n"
				+ "Steamed Milk,10\n\n" + "Sugar,10\n\n" + "Whipped Cream,10\n\n");
	}
	
	@Test
	public void printDrinkTest() {
		Assert.assertEquals(printer.generateDrinkMenu(), "Menu:\n\n"
				+ "1,Caffe Americano,3.30,true\n\n"
				+ "2,Caffe Lattee,2.55,true\n\n"
				+ "3,Caffe Mocha,3.35,true\n\n"
				+ "4,Cappuccino,3.80,true\n\n"
				+ "5,Coffee,2.75,true\n\n"
				+ "6,Decaf Coffee,2.75,true\n\n");
	}
}
