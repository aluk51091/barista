/**
 * 
 */
package lukasik.alex.barista.processor;

import lukasik.alex.barista.inventory.Drink;
import lukasik.alex.barista.inventory.Menu;
import lukasik.alex.barista.inventory.dao.impl.BaseInventoryDaoCreation;
import lukasik.alex.barista.inventory.dao.impl.InventoryDaoCachedImpl;
import lukasik.alex.barista.io.InvalidInputException;
import lukasik.alex.barista.menu.impl.ConsoleMenuPrinter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * @author Alex
 *
 */
public class IntegerProcessorTest {
	private IntegerProcessor processor;
	private InventoryDaoCachedImpl spy;

	@Before
	public void init() {
		processor = new IntegerProcessor();
		spy = Mockito.spy(BaseInventoryDaoCreation.getDao());
		processor.setInventoryDao(spy);
		ConsoleMenuPrinter printer = Mockito.mock(ConsoleMenuPrinter.class);
		processor.setMenuPrinter(printer);
		Mockito.doNothing()
				.when(printer)
				.printDispensing(Mockito.any());
	}

	@Test(expected = InvalidInputException.class)
	public void invalidInputLow() {
		processor.process(0);
	}

	@Test(expected = InvalidInputException.class)
	public void invalidInputHigh() {
		processor.process(Menu.getDrinkMenu().size() + 1);
	}

	@Test
	public void validInput() {
		Mockito.doNothing().when(spy).removeInventory(Mockito.any(Drink.class));
		// should be no exception thorwn
		processor.process(1);
		processor.process(2);
		processor.process(3);
		processor.process(4);
		processor.process(5);
		processor.process(6);
	}

	@Test(expected = InvalidInputException.class)
	public void validNumberNotInStock() {
		Mockito.doReturn(false).when(spy)
				.isDrinkInStock(Mockito.any(Drink.class));
		processor.process(1);
	}

	@Test
	public void accessorTest() {
		Assert.assertNotNull(processor.getInventoryDao());
	}
}
