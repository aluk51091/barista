/**
 * 
 */
package lukasik.alex.barista.processor;

import lukasik.alex.barista.inventory.dao.impl.BaseInventoryDaoCreation;
import lukasik.alex.barista.inventory.dao.impl.InventoryDaoCachedImpl;
import lukasik.alex.barista.io.InvalidInputException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * @author Alex
 *
 */
public class StringProcessorTest {

	private StringProcessor processor;
	private InventoryDaoCachedImpl spy;
	
	@Before
	public void init(){
		processor = new StringProcessor();
		spy = Mockito.spy(BaseInventoryDaoCreation.getDao());
		processor.setInventoryDao(spy);
		Mockito.doNothing().when(spy).restock();
	}
	
	@Test
	public void restockTest(){
		processor.process("r");
	}
	
	@Test
	public void quitTest(){
		//this cant be run because it kills the jvm
		//processor.process("q");
	}
	
	@Test(expected = InvalidInputException.class)
	public void anythingElse(){
		processor.process("asfsadf");
	}
}
