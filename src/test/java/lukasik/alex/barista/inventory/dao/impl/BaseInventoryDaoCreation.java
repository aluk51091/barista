/**
 * 
 */
package lukasik.alex.barista.inventory.dao.impl;


/**
 * @author Alex
 *
 */
public class BaseInventoryDaoCreation {
	
	public static InventoryDaoCachedImpl getDao(){
		InventoryDaoCachedImpl dao = new InventoryDaoCachedImpl();
		dao.initializeInventory();
		return dao;
	}
}
