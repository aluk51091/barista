/**
 * 
 */
package lukasik.alex.barista.inventory.dao.impl;

import java.util.Map;

import lukasik.alex.barista.inventory.Drink;
import lukasik.alex.barista.inventory.Ingredient;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * @author Alex
 *
 */
public class InventoryDaoCachedImplTest {
	private InventoryDaoCachedImpl inventoryDao;

	@Before
	public void init() {
		inventoryDao = BaseInventoryDaoCreation.getDao();
	}

	@Test
	public void removeInventoryTest() {
		inventoryDao.removeInventory(Drink.COFFEE.getIngredients());
	}

	@Test
	public void subtractInventoryTest() {
		inventoryDao.subtractIngredient(ImmutableMap.of(Ingredient.COFFEE, 3,
				Ingredient.CREAM, 1, Ingredient.SUGAR, 1));

		Assert.assertTrue(inventoryDao.getIngredientQuantity(Ingredient.COFFEE) == 7);
		Assert.assertTrue(inventoryDao.getIngredientQuantity(Ingredient.CREAM) == 9);
		Assert.assertTrue(inventoryDao.getIngredientQuantity(Ingredient.SUGAR) == 9);

	}

	@Test
	public void getIngredientQuantityTest() {
		Assert.assertTrue(inventoryDao.getIngredientQuantity(Ingredient.COFFEE) == InventoryDaoCachedImpl.maxInventory);
	}

	@Test
	public void getIngredientQuantityTestNull() {
		Assert.assertNull(inventoryDao.getIngredientQuantity(null));
	}

	@Test
	public void isDrinkInStockTestNull() {
		Assert.assertFalse(inventoryDao.isDrinkInStock(null));
	}

	@Test
	public void IsDrinkInStockTest() {
		Assert.assertTrue(inventoryDao.isDrinkInStock(Drink.COFFEE));
	}

	@Test
	public void areIngredientsInStockTestFalse() {
		Assert.assertFalse(inventoryDao.areIngredientsInStock(ImmutableList.of(
				Ingredient.COCOA,// 1
				Ingredient.COCOA,// 2
				Ingredient.COCOA,// 3
				Ingredient.COCOA,// 4
				Ingredient.COCOA,// 5
				Ingredient.COCOA,// 6
				Ingredient.COCOA,// 7
				Ingredient.COCOA,// 8
				Ingredient.COCOA,// 9
				Ingredient.COCOA,// 10
				Ingredient.COCOA // 11
				)));
	}
	
	@Test
	public void areIngredientsInStockTestNull() {
		Assert.assertFalse(inventoryDao.areIngredientsInStock(null));
	}
	
	@Test
	public void reduceListTest(){
		Map<Ingredient,Integer> reducedMap = inventoryDao.reduceList(ImmutableList.of(Ingredient.COCOA,Ingredient.COCOA));
		Assert.assertEquals(reducedMap.get(Ingredient.COCOA),(Integer)2);
	}
	
	@Test
	public void restockTest(){
		inventoryDao.removeInventory(Drink.COFFEE);
		Assert.assertEquals(inventoryDao.getIngredientQuantity(Ingredient.COFFEE),(Integer)7);
		Assert.assertEquals(inventoryDao.getIngredientQuantity(Ingredient.SUGAR),(Integer)9);
		Assert.assertEquals(inventoryDao.getIngredientQuantity(Ingredient.CREAM),(Integer)9);
		
		inventoryDao.restock();
		Assert.assertEquals(inventoryDao.getIngredientQuantity(Ingredient.COFFEE),(Integer)10);
		Assert.assertEquals(inventoryDao.getIngredientQuantity(Ingredient.SUGAR),(Integer)10);
		Assert.assertEquals(inventoryDao.getIngredientQuantity(Ingredient.CREAM),(Integer)10);
		
	}

}
