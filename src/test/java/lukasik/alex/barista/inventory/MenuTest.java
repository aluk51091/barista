/**
 * 
 */
package lukasik.alex.barista.inventory;

import lukasik.alex.barista.io.InvalidInputException;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Alex
 *
 */
public class MenuTest {

	@Test(expected=InvalidInputException.class)
	public void drinkSizeTestUnder(){
		Menu.getDrinkAt(0);
	}
	
	@Test(expected=InvalidInputException.class)
	public void drinkSizeTestOver(){
		Menu.getDrinkAt(10);
	}
	
	@Test
	public void drinkSizeTestInRange(){
		Assert.assertEquals(Menu.getDrinkAt(1),Drink.CAFFE_AMERICANO);
	}
	
	@Test
	public void drinkAccessorTest(){
		Assert.assertTrue(Menu.getDrinkMenu().size() == 6);
	}
	@Test(expected=InvalidInputException.class)
	public void ingredientSizeTestUnder(){
		Menu.getIngredientAt(0);
	}
	
	@Test(expected=InvalidInputException.class)
	public void ingredientSizeTestOver(){
		Menu.getIngredientAt(10);
	}
	
	@Test
	public void ingredientSizeTestInRange(){
		Assert.assertEquals(Menu.getIngredientAt(1),Ingredient.COCOA);
		Assert.assertEquals(Menu.getIngredientAt(2),Ingredient.COFFEE);
		Assert.assertEquals(Menu.getIngredientAt(3),Ingredient.CREAM);
		Assert.assertEquals(Menu.getIngredientAt(4),Ingredient.DECAF_COFFEE);
		Assert.assertEquals(Menu.getIngredientAt(5),Ingredient.ESPRESSO);
		Assert.assertEquals(Menu.getIngredientAt(6),Ingredient.FOAMED_MILK);
		Assert.assertEquals(Menu.getIngredientAt(7),Ingredient.STEAMED_MILK);
		Assert.assertEquals(Menu.getIngredientAt(8),Ingredient.SUGAR);
		Assert.assertEquals(Menu.getIngredientAt(9),Ingredient.WHIPPED_CREAM);
	}
	
	@Test
	public void ingredientAccessorTest(){
		Assert.assertTrue(Menu.getIngredientMenu().size() == 9);
	}
}
