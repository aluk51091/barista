/**
 * 
 */
package lukasik.alex.barista.inventory;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Alex
 *
 */
public class DrinkTest {
	@Test
	public void test(){
		Assert.assertEquals(Drink.COFFEE.getDrinkPrice(),new BigDecimal("2.75"));
	}
}
