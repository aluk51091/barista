/**
 * 
 */
package lukasik.alex.barista.utils;

import static lukasik.alex.barista.utils.StringUtilities.upperUnderscoreToCamelSpaced;

import org.junit.Assert;
import org.junit.Test;
/**
 * @author Alex
 *
 */
public class StringUtilsTest {
	
	@Test
	public void stringUtilNullTest(){
		Assert.assertNull(upperUnderscoreToCamelSpaced(null));
	}
	
	@Test
	public void stringUtilValidTest(){
		Assert.assertEquals(upperUnderscoreToCamelSpaced("Alex"),"Alex");
		Assert.assertEquals(upperUnderscoreToCamelSpaced("Alex_LUKASIK"),"Alex Lukasik");
		Assert.assertEquals(upperUnderscoreToCamelSpaced("alex__LUKASIK"),"Alex  Lukasik");
	}
	
	@Test
	public void coverageTest(){
		@SuppressWarnings("unused")
		StringUtilities u = new StringUtilities();
	}
}
