/**
 * 
 */
package lukasik.alex.barista.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Alex
 *
 */
public class IntegerUtilsTest {

	@Test
	public void isIntegerTest() {
		Assert.assertTrue(IntegerUtilities.isInteger("1"));
	}
	@Test
	public void isIntegerTestFailure() {
		Assert.assertFalse(IntegerUtilities.isInteger("a"));
	}
	
	@Test
	public void isIntegerTestNull(){
		Assert.assertFalse(IntegerUtilities.isInteger(null));
	}

	@Test
	public void parseIntegerTest() {
		Assert.assertEquals(IntegerUtilities.parseInteger("1"),(Integer)1);
	}
	
	@Test
	public void parseIntegerTesetNull(){
		Assert.assertNull(IntegerUtilities.parseInteger(null));
	}
	
	@Test
	public void parseIntegerFailure(){
		Assert.assertNull(IntegerUtilities.parseInteger("a"));
	}
	
	@Test
	public void coverageTest(){
		@SuppressWarnings("unused")
		IntegerUtilities u = new IntegerUtilities();
	}
}
