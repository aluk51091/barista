/**
 * 
 */
package lukasik.alex.barista.io;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Alex
 *
 */
public class ConsoleReaderTest {
	private ConsoleReader reader;
	@Before
	public void init(){
		reader = new ConsoleReader();
	}

	
	@Test
	public void testReader(){
		reader.sc = new Scanner(new ByteArrayInputStream("r".getBytes()));
		Assert.assertEquals(reader.read(),"r");
	}
}
